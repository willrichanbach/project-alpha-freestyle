from django.urls import path
from tasks.views import create_task, show_my_tasks, show_notes


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/notes", show_notes, name="show_notes"),
]
