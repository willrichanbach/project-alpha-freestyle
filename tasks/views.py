from django.shortcuts import render, redirect
from tasks.forms import TaskForm, TaskNoteForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task, TaskNote

# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks": my_tasks,
    }
    return render(request, "tasks/show_tasks.html", context)


@login_required
def show_notes(request, id):
    if request.method == "POST":
        form = TaskNoteForm(request.POST)
        if form.is_valid():
            note = form.save(False)
            note.assignee = request.user
            task = Task.objects.get(id=id)
            note.task = task
            note.save()
            return redirect("show_notes", id=id)
    else:
        form = TaskNoteForm()
        notes = TaskNote.objects.filter(task=id)
        context = {
            "notes": notes,
            "form": form,
        }
    return render(request, "tasks/show_notes.html", context)
