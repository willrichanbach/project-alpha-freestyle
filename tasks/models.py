from django.db import models
from projects.models import Project
from django.conf import settings

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        null=True,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class TaskNote(models.Model):
    note = models.TextField(max_length=500)
    date_added = models.DateTimeField(auto_now_add=True)
    task = models.ForeignKey(
        Task, related_name="note", on_delete=models.CASCADE
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="notes",
        null=True,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.date_added)
